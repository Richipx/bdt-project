# BDT-Project
### Irene Tafani - Riccardo Zorzoni
#
## Description
The project aims at predicting the stock trend of some countries for the next day using as predictors the stock closing price history, the Covid-19 cases and the lockdown policies. Data are obtained by the Extract, Transform and Load (ETL) part and then the Machine Learning (ML) part processes them using LSTM model. These 2 services run every 24 hours thanks to crontab.

The project is hosted on a Virtual Machine (VM) on Google Cloud Platform.

To access the results you may use one of these URLs: 
- http://34.89.228.146:5000/data?country=Italy
- http://34.89.228.146:5000/data?country=Germany
- http://34.89.228.146:5000/data?country=France
- http://34.89.228.146:5000/data?country=Spain
- http://34.89.228.146:5000/data?country=UK
- http://34.89.228.146:5000/data?country=China
- http://34.89.228.146:5000/data?country=Japan
- http://34.89.228.146:5000/data?country=US


## Architecture
- The `docker-compose.yml` orchestrates the microservices
- The `etl` directory is composed by:
  - `initPG.py` which creates the tables in the PostgreSQL;
  - `covid1.py` which downloads the Covid-19 data from Github and puts them into Redis using sorted set
  - `covid2.py` which gets data from Redis and puts them into PostgreSQL
  - `stock1.py` which downloads the stock data from yfinance API and through a scrape part in Selenium and puts them into Redis using sorted set
  - `stock2.py` which gets data from Redis and puts them into PostgreSQL 
  - `lock1.py` which downloads the lockdown data from Github and puts them into Redis
  - `lock2.py` which gets data from Redis and puts them into PostgreSQL
  - `cronetl` which controls the calls order for cron
  - `Dockerfile` which defines the steps to build the containerized version of the ETL part
  - `requirements.txt` which defines the python library for the Dockerfile
  - `cron_etl` which is the cron job, every day at 6:00 UTC, from Monday to Friday
- The `ml` directory is composed by:
  - `processing.py` which retrieves the data from PostgreSQL and it processes them thanks to Machine Learning model written in TensorFlow, in particular using an LSTM model which is the most suitable algorithm for time series. Finally, it stores the results into Redis
  - `cronml` which controls the calls order for cron
  - `Dockerfile` which defines the steps to build the containerized version of the ML part
  - `requirements.txt` which defines the python library for the Dockerfile
  - `cron_ml` which is the cron job, every day at 6.15 UTC, from Monday to Friday
- The `flask` directory is compesed by:
  - `forFlask.py` which gets the results from Redis
  - `apiFlask.py` which answers to the users' requests through API written in Flask
  - `Dockerfile` which defines the steps to build the containerized version of the API part 
  - `requirements.txt` which defines the python library for the Dockerfile
- The `database.env` defines the environment variable for the PostgreSQL database


## Quick Start
Steps: 
- install `docker` and `docker-compose`
- clone the repository
- lauch the docker-compose command `docker-compose up`
- visualize the data on `localhost:5000/data?country=X` where X must be a country of this list: 
    - Italy, Germany, France, Spain, UK, China, Japan, US


## Screenshots
A picture which illustrates the architecture
![alt text][img1]
A graph which shows how the LSTM model approximates the real function of closing price
![alt text][img2]

[img1]: images/architecture.png "Architecture"
[img2]: images/trend.jpg "Trend"

## License
This project is licensed under the terms of the Apache license.