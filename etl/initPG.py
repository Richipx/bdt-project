import psycopg2


# creation of table in postgres
def init(type):
    try:
        connection = psycopg2.connect( user="postgres",
                                    password="richirene",
                                    host="postgres",
                                    port="5432",
                                    database="BDT2020")

        cursor = connection.cursor()

        if (type == 'covid') or (type == 'all'):
    
            create_table_query = '''CREATE TABLE IF NOT EXISTS covidTable(
            ID      DATE PRIMARY KEY NOT NULL,
            GERMANY    INT,
            FRANCE INT,
            SPAIN    INT,
            UK    INT,
            JAPAN INT,
            CHINA INT,
            US INT,
            ITALY INT);
            '''

            cursor.execute(create_table_query)
            connection.commit()
            print("Table covid created successfully in PostgresSQL ")

        if (type == 'stock') or (type == 'all'):
    
            create_table_query = '''CREATE TABLE IF NOT EXISTS stockTable(
            ID      DATE PRIMARY KEY NOT NULL,
            GERMANY    INT,
            FRANCE INT,
            SPAIN    INT,
            UK    INT,
            JAPAN INT,
            CHINA INT,
            US INT,
            ITALY INT);
            '''

            cursor.execute(create_table_query)
            connection.commit()
            print("Table stock created successfully in PostgresSQL ")

        if (type == 'lock') or (type == 'all'):
    
            create_table_query = '''CREATE TABLE IF NOT EXISTS lockTable(
            ID      DATE PRIMARY KEY NOT NULL,
            CHINA    INT,
            GERMANY INT,
            SPAIN    INT,
            FRANCE    INT,
            UK INT,
            ITALY INT,
            JAPAN INT,
            US INT);
            '''

            cursor.execute(create_table_query)
            connection.commit()
            print("Table lock created successfully in PostgresSQL ")

        

    except (Exception, psycopg2.DatabaseError) as error :
        print ("Error in transction Reverting all other operations of a transction ", error)
        connection.rollback()

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

# if is necessary, a simple function to delete table/tables
def delete(type):
    try:
        connection = psycopg2.connect( user="postgres",
                                    password="richirene",
                                    host="172.17.0.3",
                                    port="5432",
                                    database="BDT2020")

        cursor = connection.cursor()

        if (type == 'covid') or (type == 'all'):
            delete_table_query = '''DROP TABLE covidTable; '''
            cursor.execute(delete_table_query)
            connection.commit()
            print("Table covid deleted successfully in PostgresSQL ")

        if (type == 'stock') or (type == 'all'):
            delete_table_query = '''DROP TABLE stockTable; '''
            cursor.execute(delete_table_query)
            connection.commit()
            print("Table stock deleted successfully in PostgresSQL ")

        if (type == 'lock') or (type == 'all'):
            delete_table_query = '''DROP TABLE lockTable; '''
            cursor.execute(delete_table_query)
            connection.commit()
            print("Table lock deleted successfully in PostgresSQL ")


    except (Exception, psycopg2.DatabaseError) as error :
        print ("Error in transction Reverting all other operations of a transction ", error)
        connection.rollback()

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


#init('all')

# caution!
#delete('#all') #noo