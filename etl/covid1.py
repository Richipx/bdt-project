import pandas as pd
import redis
import datetime
import time
import json

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)


# data aquisition
def covid():
    # last day 
    last = r.get('last_covid')
    if last is None:
        last = pd.to_datetime('20200122', format='%Y%m%d', errors='ignore')
    else:
        last = last.decode('UTF-8')
        last = last[:10]
        last = pd.to_datetime(last, format='%Y%m%d', errors='ignore')
    
    # data from github - COVID-19 Data Repository by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University - https://github.com/CSSEGISandData/COVID-19
    url = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
    df = pd.read_csv(url, error_bad_lines=False, index_col=False)
    
    # data manipolation 
    df = df.drop(['Province/State','Lat','Long'],1)
    df = df.groupby(['Country/Region'], as_index = False ).sum()
    df = df.rename(columns={"Country/Region":"Country"})
    df = df.set_index('Country')
    df = df.transpose()
    df.index = pd.to_datetime(df.index)
    df = df[['Germany','France','Spain','United Kingdom','Japan', 'China','US','Italy']]
    
    # only data which are not in the db
    new = df.loc[df.index > last]

    # data in redis
    if not new.empty:
        COVIDinredis(new)
    

# function for redis
def COVIDinredis(df):
    df.index = df.index.astype('str')
    ll = df.index[-1]
    tuples = list(df.itertuples(index=True, name=None))
    for i in tuples:
        score = i[0]
        score_unix = time.mktime(datetime.datetime.strptime(score, '%Y-%m-%d').timetuple())
        value = list(i[0:])
        value_json = json.dumps(value)
        # sorted set with score
        r.zadd("covid", {value_json: score_unix})
    ll = ll.replace('-','')
    ll = pd.to_datetime(ll, format='%Y%m%d', errors='ignore')
    # update the last variable
    redis_date('last_covid', str(ll))
    

def redis_date(key_date, value_last):
    r.set(key_date, value_last)


