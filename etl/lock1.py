import pandas as pd
import redis
import datetime
import time
import json

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)



# data aquisition
def lock():
    
    # last day 
    last = r.get('last_lock')
    if last is None:
        last = pd.to_datetime('20200122', format='%Y%m%d', errors='ignore')
    else:
        last = last.decode('UTF-8')
        last = last[:10]
        last = pd.to_datetime(last, format='%Y%m%d', errors='ignore')

    # data from github - Thomas Hale, Sam Webster, Anna Petherick, Toby Phillips, and Beatriz Kira. (2020). Oxford COVID-19 Government Response Tracker. Blavatnik School of Government.
    df = pd.read_csv("https://raw.githubusercontent.com/OxCGRT/covid-policy-tracker/master/data/timeseries/c6_stayathomerequirements.csv", encoding='UTF-8')
    df.rename(columns={'Unnamed: 0': 'Country', 'Unnamed: 1': 'Country_code'}, inplace=True)
    df = df.loc[df['Country'].isin(["Italy", "Germany", "France", "Spain", "Japan","China", "United Kingdom", "United States"])]
    del df['Country_code']
    df = df.set_index('Country').T.rename_axis('Date').reset_index()
    df = df.replace('.', None)
    df = df.fillna(method='ffill')
    df['Date'] = pd.to_datetime(df.Date)
    df = df[  df.Date > last  ]
    df = df.set_index('Date')

    if not df.empty:
        LOCKinredis(df)  

# function for redis
def LOCKinredis(df):
    df.index = df.index.astype('str')
    ll = df.index[-1]
    tuples = list(df.itertuples(index=True, name=None))
    for i in tuples:
        score = i[0]
        score_unix = time.mktime(datetime.datetime.strptime(score, '%Y-%m-%d').timetuple())
        value = list(i[0:])
        value_json = json.dumps(value)
        # sorted set with score
        r.zadd("lock", {value_json: score_unix})
    ll = ll.replace('-','')
    ll = pd.to_datetime(ll, format='%Y%m%d', errors='ignore')
    # update the last variable
    redis_date('last_lock', str(ll))

def redis_date(key_date, value_last):
    r.set(key_date, value_last)




