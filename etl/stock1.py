import yfinance as yf
import pandas as pd
import redis

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options
import time
from lxml import html

import datetime
import json

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)

countries = ['^GDAXI',
             '^FCHI',
             '^IBEX',
             '^FTSE',
             '^N225',
             '000001.SS',
             '^DJI'
]


# data aquisition
def stock():

    # last day 
    last = r.get('last_stock')
    if last is None:
        last = pd.to_datetime('20200122', format='%Y%m%d', errors='ignore')
    else:
        last = last.decode('UTF-8')
        last = last[:10]
        last = pd.to_datetime(last, format='%Y%m%d', errors='ignore')

    # utilization of yfinance library for stock data
    data = yf.download(countries, start=last)
    data = data[['Close']]
    d = {
        'DE':data["Close"]["^GDAXI"],
        'FR':data["Close"]["^FCHI"],
        'ESP':data["Close"]["^IBEX"],
        'UK':data["Close"]["^FTSE"],
        'JP':data["Close"]["^N225"],
        'CN':data["Close"]["000001.SS"],
        'US':data["Close"]["^DJI"],
    }
    df = pd.DataFrame(data=d)
    df = df.ffill()

    # Italy is not in yfinance data
    # so scrape function allow to aquire these data
    dfi = scrape(last)

    df = pd.merge(df, dfi, left_index=True, right_index=True, how='inner')
    df = df.astype(int)
    
    # data in redis
    STOCKinredis(df)

# function for scrape data
def scrape(ll):

    ll = str(ll)
    ll = ll[:10]
    ll = '/'.join(ll.split('-')[::-1]) #from 2020-01-22 to 22/01/2020

    # investing for italian data
    url = "https://it.investing.com/indices/it-mib-40-historical-data"

    # headless connection
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options)
    driver.get(url)

    # wait page loading
    driver.implicitly_wait(10)

    # click on date menu
    frame = driver.find_element_by_id('widgetFieldDateRange')
    driver.execute_script("arguments[0].click();", frame)

    time.sleep(3)

    # remove old date
    st = driver.find_element_by_id("startDate")
    for i in range(10):
        st.send_keys(Keys.BACKSPACE)

    time.sleep(3)

    # insert date of interest
    st.send_keys(ll) 

    time.sleep(3)

    # confirm the range
    appl = driver.find_element_by_id('applyBtn')
    driver.execute_script("arguments[0].click();", appl)

    # get the table from html
    #tbody = driver.find_element_by_xpath("/html/body/div[6]/section/div[9]/table[1]/tbody")
    tbody = driver.find_element_by_xpath("/html/body/div[5]/section/div[9]/table[1]/tbody")
    table = tbody.get_attribute('innerHTML')

    # close the connection
    time.sleep(3)
    driver.quit()

    # data manipulation
    xml = html.fromstring(table)

    l = []
    for row in xml.xpath(".//tr"):
        l.append([td.text for td in row.xpath(".//td[text()]")])
    
    dfi = pd.DataFrame(l[::-1])

    dfi = dfi[[0,1]]
    dfi[0] = dfi[0].apply(lambda x: '-'.join(x.split('.')[::-1]) )

    dfi = dfi.rename(columns={0: 'date', 1: "IT"})
    dfi = dfi.set_index('date')

    dfi['IT'] = dfi['IT'].apply(lambda x: int(float(x.replace('.','').replace(',','.') )) )

    return dfi

# function for redis
def STOCKinredis(df):
    df.index = df.index.astype('str')
    ll = df.index[-1]
    tuples = list(df.itertuples(index=True, name=None))
    for i in tuples:
        score = i[0]
        score_unix = time.mktime(datetime.datetime.strptime(score, '%Y-%m-%d').timetuple())
        value = list(i[0:])
        value_json = json.dumps(value)
        # sorted set with score
        r.zadd("stock", {value_json: score_unix})
    ll = ll.replace('-','')
    ll = pd.to_datetime(ll, format='%Y%m%d', errors='ignore')
    # update the last variable
    redis_date('last_stock', str(ll))

def redis_date(key_date, value_last):
    r.set(key_date, value_last)





