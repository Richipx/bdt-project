import redis
import ast
import datetime
import time
import json
import psycopg2

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)

# function for insert data in postegres database
def COVIDinsert_postgres(records):
    try:
        connection = psycopg2.connect(user = "postgres",
                                    password = "richirene",
                                    host = 'postgres',
                                    port = "5432",
                                    database = "BDT2020")

        cursor = connection.cursor()
        sql_insert_query = """ INSERT INTO covidTable 
                                (ID, GERMANY, FRANCE, SPAIN, 
                                 UK, JAPAN, CHINA, US, ITALY ) 
                                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) """
        
        # executemany() to insert multiple rows
        # but we prefer to insert one row a time
        cursor.executemany(sql_insert_query, records)
        connection.commit()
        print(cursor.rowcount, "Record inserted successfully into table")

               
    except (Exception, psycopg2.Error) as error :
        print ("Error postgresSQL:", error)
    finally:
        #closing database connection.
        if (connection):
            cursor.close()
            connection.close()
            print("postgresSQL connection is closed")


def covidtopg():
    try:
        while (a := r.bzpopmin('covid', timeout=5)) is not None:
            a = a[1].decode('UTF-8')
            x = ast.literal_eval(a)
    
            x = tuple(x)
        
            COVIDinsert_postgres([x]) 

            time.sleep(1)
    except Exception as e:
        print(e)


# while True: 
#     try:
#         # wait 24h if there is no data in redis sorted set
#         a = r.bzpopmin(data,timeout=86400)
#         a = a[1].decode('UTF-8')
#         x = ast.literal_eval(a)
    
#         x = tuple(x)
        
#         insert_postgres([x]) 

#         time.sleep(10)
        
#     except Exception as e:
#         print(e)
