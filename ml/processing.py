import pandas as pd
import psycopg2

import math
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM, Flatten
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')

from sklearn.metrics import confusion_matrix

from datetime import datetime
import redis
import json

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)

# retrive data of interest from postgres
def query(country):
    try:
        connection = psycopg2.connect(user="postgres",
                                     password="richirene",
                                     host='postgres',
                                     port="5432",
                                     database="BDT2020")

        # 1. retrive covid data
        cursor = connection.cursor()
        postgreSQL_select_Query = "select id, "+country+" from covidTable"
        cursor.execute(postgreSQL_select_Query)
        records = cursor.fetchall()
        l = []
        for row in records:
            l.append(row)
        df_covid = pd.DataFrame(l, columns =['Date', 'confirmed']) 
        df_covid = df_covid.set_index('Date')

        # 2. retrive stock data
        cursor = connection.cursor()
        postgreSQL_select_Query = "select id, "+country+" from stockTable"
        cursor.execute(postgreSQL_select_Query)
        records = cursor.fetchall()
        l = []
        for row in records:
            l.append(row)
        df_stock = pd.DataFrame(l, columns =['Date', 'value']) 
        df_stock = df_stock.set_index('Date')

        # 3. retrive lockdown data
        cursor = connection.cursor()
        postgreSQL_select_Query = "select id, "+country+" from lockTable"
        cursor.execute(postgreSQL_select_Query)
        records = cursor.fetchall()
        l = []
        for row in records:
            l.append(row)
        df_lock = pd.DataFrame(l, columns =['Date', 'lockdown']) 
        df_lock = df_lock.set_index('Date')

        # df_lock = df_lock.replace(1,0)
        # df_lock = df_lock.replace(2,1)
        # df_lock = df_lock.replace(3,1)


        df = pd.merge(df_stock, df_covid, left_index=True, right_index=True, how='inner')
        df = pd.merge(df, df_lock, left_index=True, right_index=True, how='inner')

        # calculate the variation in confirmed covid data
        df['confirmed'] = df['confirmed'].diff(1)
        df = df.dropna()
        return(df)
        
    except (Exception, psycopg2.Error) as error :
        print ("Error while fetching data from PostgreSQL: ", error)

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

# machine learning with LSTM algorithm
def ml(df):
    dataset = df.values

    # nr of row to train the model 
    training_data_len = math.ceil(len(dataset) * 0.85)

    # days for LSTM algorithm
    days = 22

    # training ds
    # scaled train ds
    train_data = dataset[0:training_data_len , :]
    test_data = dataset[training_data_len-days: , :]

    scaler = MinMaxScaler(feature_range=(0,1))

    scaled_train_data = scaler.fit_transform(train_data)
    scaled_test_data = scaler.transform(test_data)

    # split the data into x_train and y_train ds
    x_train = []
    y_train = []

    x_test = []
    y_test = dataset[training_data_len:, 0]

    for i in range (days, len(train_data)):
        x_train.append(scaled_train_data[i-days:i, :])
        y_train.append(scaled_train_data[i, 0])

    for i in range(days, len(test_data)):
        x_test.append(scaled_test_data[i-days:i, :])
        
    # convert x_train and y_train to numpy array
    x_train, y_train = np.array(x_train), np.array(y_train)

    # reshape data
    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 3))  

    # built the LSTM model
    # 4 layers, 2 LSTM and 2 Dense
    model = Sequential()
    model.add(LSTM(50, return_sequences=True, input_shape=(x_train.shape[1], 3)))
    model.add(LSTM(50, return_sequences=False))
    model.add(Dense(20))
    model.add(Dense(1))

    # compile the model
    model.compile(optimizer='adam', loss='mean_squared_error')

    # train the model
    model.fit(x_train, y_train, batch_size=1, epochs=30)  

    # convert the data to a numpy array
    x_test = np.array(x_test)

    # reshape the data 
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 3)) 

    # get the models predicted price values
    predictions = model.predict(x_test)

    z = np.zeros((x_test.shape[0],1))
    predictions = np.append(predictions, z, axis=1)
    predictions = np.append(predictions, z, axis=1)
    predictions = scaler.inverse_transform(predictions)

    # get the root mean squared error (RMSE)
    rmse = np.sqrt( np.mean(( predictions[:,0] - y_test )**2 ))
    # print('RMSE: ', rmse)

    # plot the data
    train = df[:training_data_len]
    valid = df[training_data_len:]
    valid['Predictions'] = predictions[:,0]

    # #VISUALIZE THE DATA
    # plt.figure(figsize=(16,8))
    # plt.title(country)
    # plt.xlabel('Date', fontsize=18)
    # plt.ylabel('Close Price', fontsize=18)
    # plt.plot(train['value'])
    # plt.plot(valid[['value', 'Predictions']])
    # plt.legend(['Train', 'Val', 'Predictions'], loc='lower right')
    # plt.show()  

    # #ACCURACY OF THE MODEL
    # res = valid[['value','Predictions']]
    # res = res.diff(1).dropna()
    # res['value'] = res['value'].apply(lambda x: 'UP' if x > 0 else 'DOWN')
    # res['Predictions'] = res['Predictions'].apply(lambda x: 'UP' if x > 0 else 'DOWN')
    # conf = confusion_matrix(res.value, res.Predictions)
    # perc = (conf[0][0]+conf[1][1])/sum(sum(conf))*100

    # try to predict the next day using last 22 days
    # get the last 22 days closing price values and convert the df to an array
    last_days = df[-days:].values
    # scale the data to be values between 0 and 1
    last_days_scaled = scaler.transform(last_days)
    # create and empty list
    X_test = []
    # append the past 22 days
    X_test.append(last_days_scaled)
    # convert the X_test data set to a numpy array
    X_test = np.array(X_test)
    # reshape the data
    X_test = np.reshape(X_test, (X_test.shape[0],X_test.shape[1], 3))
    # get the predicted scaled price
    pred_price = model.predict(X_test)
    # undo the scaling
    z = np.zeros((X_test.shape[0],1))
    pred_price = np.append(pred_price, z, axis=1)
    pred_price = np.append(pred_price, z, axis=1)
    pred_price = scaler.inverse_transform(pred_price)

    last_day = df.index[-1]
    last_day = last_day.strftime("%Y-%b-%d")
    
    df2 = df.iloc[[-1]]
    last_value = df2.iloc[0]['value']

    if pred_price[0][0] > last_value:
        res_d = {'day after': last_day, 'result:': 'UP'}
    else:
        res_d = {'day after': last_day, 'result:': 'DOWN'}

    return res_d

# store the results in redis, so retriving them is fast
def in_redis(key_country, value_res):
    r.set(key_country, value_res)


countries = ['Germany','France','Spain','UK','Japan','China','US','Italy']

# retrive, process, and store results on redis
def all_countries(countries=countries):
    for country in countries:
        res_dict = ml(query(country))
        res_dict_json = json.dumps(res_dict)
        in_redis(country, res_dict_json)



