import redis
import ast

# define redis connection
r = redis.Redis(host='redis', port=6379,db=0)

# get selected country from redis
def flask_redis(cou):
    res = r.get(cou)
    try:
        res = res.decode('UTF-8')
        y = ast.literal_eval(res)
        return y
    except:
        err = 'This index does not exist, maybe try with a capital first letter'
        return err

    




