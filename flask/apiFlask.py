from flask import Flask
import forFlask as ff
from flask import request

app = Flask(__name__)

# 127.0.0.1:5000/data?country=Italy

@app.route('/data')
def data():
    country = request.args.get('country')
    return ff.flask_redis(country)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000', debug=True)
    
    
